# Arduino PID fan temperature controller

PID (Proportional-Integral-Derivative) fan controller intended to maintain a
constant temperature in a place. Inspired by the idea of improving the
performance of caravan fridges by maintaining a constant temperature in
the evaporation system.

This project is unfinished and highly experimental, including ugly code and
poor documentation. Yet I hope you find it useful for your own projects.

Watch the demo video.

![Demo video](https://www.youtube.com/watch?v=py9zqRInpiY)

## Features

* Temperature is read trough an anolog input from a LM35 temperature
  sensor.
* The controller will apply power from 0% to 100% to a fan.
* Power is regulated through a PWM output. External circuitry (JFET) is used to
  modulate the voltage applied to the fan.
* Status is displayed in a 16x2 LCD display. Also through the serial port
  if connected by USB. The later includes PID components for debugging.
* Buttons allow to increase, decrease and save the target temperature to    
  EEPROM.

## Resources

  * [ARP control](https://www.arprv.com)

## Hardware

Main hardware parts

* Arduino micro board
* DC 5V/12V computer fan
* LM35 temperature sensor
* NTD N-channel 3055L 104G MOSFET (fan driver)
* 1K resistor (fan driver)
* 1N4148 diode (JFET switching protection)
* 16x2 LCD display
* Potentiometer (controls display brightness)
* Push buttons
* LED and 220Ohm resistor (PWM feedback)

## Authors

* Jorge Juan-Chico

## Licence

This is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version. See
http://www.gnu.org/licenses/.
