// Project: Fan temperature controller
//
// Description: PID fan controller intended to maintain a constant temperature
//      in an area.
//
// Main characteristics:
//
//   * Temperature is read trough an anolog input from a LM35 temperature
//     sensor.
//   * The controller will apply power from 0% to 100% to a fan.
//   * Power is regulated through a PWM output. External circuitry (like a
//     JFET) is used to modulate the voltage applied to the fan.
//   * Status is displayed in a 16x2 LCD display. Also through the serial port
//     if connected by USB. The later includes PID components for debugging.
//   * Buttons allow to increase, decrease and save target temperature to
//     EEPROM.

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. You can access a copy of the GNU General Public License //
// at <http://www.gnu.org/licenses/>.                                         //
////////////////////////////////////////////////////////////////////////////////

#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <EasyButton.h>
#include <TimerOne.h>
#include <SimpleTimer.h>

  ////////////////////////////////////
 ////  Constants and parameters  ////
////////////////////////////////////

const char versionString[] = "0.0.1";

// Debug mode: print algorithm status through the serial port
unsigned int debug = 1;
unsigned int repeatStatusHeader = 100;  // Repeat status header every ... (0-no)
unsigned int statusCount = 1;

// IO pins
const int output = 10;         // PWM output
const int buttonSelectPin = 7; // button select (currently, save to EEPROM)
const int buttonUpPin = 8;     // button up
const int buttonDownPin = 9;   // button down
const int tempPin = A0;        // temperature sensor input

// PWM parameters

// PWM cycle in us
//     us    Freq
//     40    25 KHz   - PWM-controlled fan
//    100    10 KHz
//   1000     1 KHz
//  10000   100 Hz
float cycle = 40;
float dutyMin = 0.0;        // Minimum duy cycle value (some PWM fans need this)
float dutyMax = 100.0;      // Maximum duty cycle value
float duty = dutyMin;       // Initial duty cycle in %
bool dutyInv = false;       // True - invert duty output

// Control parameters
float temp;                 // Filtered (pre-processed) temperature
float tempRaw;              // Temperature read from sensor
float temp0;                // Previous filtered temperature
const float tempMax = 100;  // Maximum allowed target temperature
const float tempMin = 10;   // Minimum allowed target temperature
float targetTemp = 35.0;    // Initial target temperature
float targetTemp0 = targetTemp;    // Target temperature in the previous cycle

// Sample PID control values
// Interval   kp   ki   kd   applications  stars (* good ** very good *** excelent)
// ---------------------------------------------
//   5000   25  1.2   20  -  small fan in a box (**)
//  10000    5  0.8   10
//  10000   10  0.8   40
//   5000   20  0.4   20  -  fridge
//   5000   10  0.4   10  -  fridge, box (**)
//   5000   15  0.3   20  -
const int controlInterval = 5000; // Control loop time interval in ms
float kp = 30;            // Proportional factor power% / C    (5 - 40)
float ki = 0.2;           // Integral factor power%/(C * s)    (0.1 - 2)
float kd = 20;            // Derivative factor power% * s / C  (0 - 40)
float softTemp = 0.4;     // Teemperature softening factor 0<softTemp<=1 (1 no soft)
float delta;              // temp - targetTemp
float dutyInt = 0;        // Integral component
float dutyProp;           // Proportional component
float dutyDer;            // Derivative component
float propMax = 100.0;    // Maximum proportional control component
float intMax = 100.0;     // Maximum integral control component
float derMax = 100.0;     // Maximum derivative control component
float duty0 = 0;          // Duty value in the previous cycle
float dutyKick = 50;      // Minimum duty to start fan from stop
float dutyOff = 3;        // Minimum duty that makes the fan to stop moving
float dutyOn = 8;         // Minimum duty value to force the fan to run
unsigned int fanOff = 0;  // Guessed fan state 1 - fan is off

  ///////////////////////////
 ////  Initial objects  ////
///////////////////////////

// Button objects
EasyButton buttonUp(buttonUpPin, NULL, CALL_NONE, true);
EasyButton buttonDown(buttonDownPin, NULL, CALL_NONE, true);
EasyButton buttonSelect(buttonSelectPin, NULL, CALL_NONE, true);

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Timed functions timer
SimpleTimer timer;

  ////////////////////////////
 ////  Helper functions  ////
////////////////////////////

float getTempLM35(int analogPin)
{
  int value = analogRead(analogPin);
  float millivolts = value * 1100.0 / 1023.0;
  return millivolts / 10.0;
}

  ////////////////////////////
 ////  EEPROM functions  ////
////////////////////////////

// Map
//   00 - targetTemp
//   ...

void eepromSave()
{
  EEPROM.write(0,int(targetTemp));
  if(debug)
    Serial.println("Values saved to EEPROM.");
}

void eepromLoad()
{
  // Target temperature
  int val = EEPROM.read(0);
  if (val >= tempMin && val <= tempMax) {
    targetTemp = float(val);
    targetTemp0 = targetTemp;
  }
}

  ////////////////////////////////////////
 ////  Display and status functions  ////
////////////////////////////////////////

// Print status/debug information through a previously initialized serial port

void printStatusHeader()
{
  Serial.println("tempRaw temp targetT delta dutyP dutyI dutyD duty fan");
}

void printStatus()
{
  if (repeatStatusHeader > 0) {
    statusCount += 1;
    if (statusCount > repeatStatusHeader) {
      printStatusHeader();
      statusCount = 1;
    }
  }

  Serial.print(tempRaw);
  Serial.print(" ");
  Serial.print(temp);
  Serial.print(" ");
  Serial.print(targetTemp);
  Serial.print(" ");
  Serial.print(delta);
  Serial.print(" ");
  Serial.print(dutyProp);
  Serial.print(" ");
  Serial.print(dutyInt);
  Serial.print(" ");
  Serial.print(dutyDer);
  Serial.print(" ");
  Serial.print(duty);
  Serial.print(" ");
  if (fanOff == 1)
    Serial.print("off");
  else
    Serial.print("on");
  Serial.println("");
}

// Refresh LCD display data
void refreshDisplay()
{
  lcd.home();
  lcd.print("Temp: ");
  lcd.print(round(temp));
  lcd.print(" > ");
  lcd.print(round(targetTemp));
  lcd.print(" C ");
  lcd.setCursor(0,1);
  lcd.print("Fan power: ");
  lcd.print(round(duty));
  lcd.print("%  ");
}

  /////////////////////////////
 ////  Control functions  ////
/////////////////////////////

////  PID control loop  ////

void controlLoopPID()
{
  // Read sensor temperature
  tempRaw = getTempLM35(tempPin);
  // Pre-process raw temperature. Average with previous temp using a
  // softening factor
  temp = tempRaw * softTemp + temp0 * (1 - softTemp);

  //
  delta = temp - targetTemp;

  // Calculate proportional component
  dutyProp = constrain(kp * delta, -propMax, propMax);
  // Experiment: cuadratic proportional component: faster response for
  // high deltas
  // dutyProp = kp/2.0 * delta * (1 + abs(delta));
  // dutyProp = constrain(dutyProp, -propMax, propMax);

  // Integral component
  dutyInt = dutyInt + ki * (controlInterval/1000.0) * delta;
  dutyInt = constrain(dutyInt, 0.0, intMax);

  // Derivative component
  dutyDer = kd * (temp - temp0) / controlInterval * 1000;
  dutyDer = constrain(dutyDer, -derMax, derMax);

  // Combine all control components
  duty = dutyProp + dutyInt + dutyDer;

  // Experiment: make derivative comp. be stored in integral part
  //dutyInt = dutyInt + dutyDer;
  //duty = dutyProp + dutyInt;

  // Duty limits
  duty = constrain(duty, dutyMin, dutyMax);

  // Experiment: trim integral component when applying duty limits
  //  if (duty > dutyMax) {
  //    dutyInt = dutyInt - (duty - dutyMax);
  //    duty = dutyMax;
  //  } else if (duty < dutyMin) {
  //    dutyInt = dutyInt - duty;
  //    duty = dutyMin;
  //  }

  // Force fan start if we guess fan was stopped
  if (fanOff == 1 && duty > dutyOn) {
    duty = dutyKick;
    fanOff = 0;
  }

  // Guess if fan have stopped
  if (duty < dutyOff)
    fanOff = 1;

  // Save current algorithm values for next loop
  duty0 = duty;
  temp0 = temp;
  targetTemp0 = targetTemp;

  // Invert duty cycle if necessary
  float dutyOut = dutyInv ? 100.0 - duty : duty;

  // Update PWM output timer
  Timer1.setPwmDuty(output, dutyOut / 100.0 * 1023.0);

  // Refresh display
  refreshDisplay();

  // Print debug information on terminal
  if (debug)
    printStatus();
}

////  Sample simple control loop  ////
// Increase duty cycle (fan speed) when temp > targetTemp,
// decrease duty cycle (fan speed) when temp < targetTemp.
void controlLoopSimple()
{
  temp = getTempLM35(tempPin);
  if (temp > targetTemp)
    duty = duty < dutyMax ? duty + 1 : duty;
  else
    duty = duty > 0 ? duty - 1 : duty;

  Timer1.setPwmDuty(output, duty / 100.0 * 1023.0);
}

////  Sample simple control loop imitating a thermostat  ////
void controlLoopThermostatic()
{
  temp = getTempLM35(tempPin);
  if (temp > targetTemp + 0.5)
    duty = dutyMax;
  else if (temp < targetTemp - 0.5)
    duty = 0.0;

  Timer1.setPwmDuty(output, duty / 100.0 * 1023.0);
  refreshDisplay();
  // printStatus();
}

  /////////////////
 ////  Setup  ////
/////////////////

void setup()
{
  // Set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Starting...");

  // Serial configuration
  Serial.begin(9600);
  Serial.println("Starting...");

  // Load EEPROM saved values
  eepromLoad();

  // Set analog reference to internal 1.1V
  analogReference(INTERNAL);

  pinMode(output, OUTPUT);
  // Initial trigger: max speed for 2 s
  digitalWrite(output, HIGH);
  delay(2000);

  // Set timer-based pwm
  Timer1.initialize(cycle);
  Timer1.pwm(output, duty / 100.0 * 1023.0);

  // Set callback
  //Timer1.attachInterrupt(callback)

  // Control setup
  temp0 = getTempLM35(tempPin);
  temp = temp0;
  timer.setInterval(controlInterval, controlLoopPID);
  // timer.setInterval(controlInterval, controlLoopThermostatic);

  // Print data header
  if (debug)
    printStatusHeader();

}

  /////////////////////
 ////  Main loop  ////
/////////////////////

void loop()
{
  // Check button press
  buttonUp.update();
  buttonDown.update();
  buttonSelect.update();

  if(buttonUp.IsPushed() && targetTemp < tempMax) {
    targetTemp0 = targetTemp;
    targetTemp = targetTemp + 1;
    refreshDisplay();
  }
  if(buttonDown.IsPushed() && targetTemp > tempMin) {
    targetTemp0 = targetTemp;
    targetTemp = targetTemp - 1;
    refreshDisplay();
  }
  if(buttonSelect.IsPushed()) {
    eepromSave();
  }

  // Poll timer
  timer.run();
}
